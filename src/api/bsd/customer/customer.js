/*
 * @Descripttion: 客户接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 14:36:21
 */
import axios from '@/utils/request'
const baseUrl = '/customer'

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/saveData`,
    method: 'post',
    data: params
  })
}

export function delData(id) {
  return axios.request({
    url: `${baseUrl}/delete/${id}`,
    method: 'post',
    data: id
  })
}

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function getOptions(params) {
  return axios.request({
    url: `${baseUrl}/listOptions`,
    method: 'post',
    data: params
  })
}

export function getDebtById(id) {
  return axios.request({
    url: `${baseUrl}/getDebt/${id}`,
    method: 'post',
    data: id
  })
}

export function getdataById(id) {
  return axios.request({
    url: `${baseUrl}/getData/${id}`,
    method: 'post',
    data: id
  })
}

export function genBillcode(params) {
  return axios.request({
    url: `${baseUrl}/getNo`,
    method: 'post',
    data: params
  })
}

export function downloadExcleTemp(params) {
  return axios.request({
    url: `${baseUrl}/downloadExcleTemp`,
    method: 'post',
    data: params,
    responseType: 'blob'
  })
}

export function exportData(params) {
  return axios.request({
    url: `${baseUrl}/export`,
    method: 'post',
    data: params,
    responseType: 'blob'
  })
}


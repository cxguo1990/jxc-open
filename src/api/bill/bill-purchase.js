/*
 * @Descripttion: 票据接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 13:01:18
 */
import axios from '@/utils/request'
const baseUrl = '/bill/purchase'

/**
 * 添加进货单（要进库存）
 * @param {*} params
 */
export function addPurchase(params) {
  return axios.request({
    url: `${baseUrl}/addData`,
    method: 'post',
    data: params
  })
}

export function cancleBill(params) {
  return axios.request({
    url: `${baseUrl}/cancle`,
    method: 'post',
    data: params
  })
}

/**
 * 查询待退货单
 * @param {} params
 */
export function getWaiteReturnPurchase(params) {
  return axios.request({
    url: `${baseUrl}/list_waite2return`,
    method: 'post',
    data: params
  })
}

/**
 * 查询进货列表
 * @param {*} params
 */
export function listPurchase(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function getBillAndDetail(params) {
  return axios.request({
    url: `${baseUrl}/getBillAndDetails/${params}`,
    method: 'post',
    data: params
  })
}

/**
 * 生成票据编号
 * @param {*} params
 */
export function getBillcode(params) {
  return axios.request({
    url: `${baseUrl}/getBillNo`,
    method: 'post',
    data: params
  })
}

export function print(params) {
  return axios.request({
    url: `${baseUrl}/print`,
    method: 'post',
    data: params
  })
}

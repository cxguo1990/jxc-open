/*
 * @Descripttion: 商品接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 13:24:15
 */
import axios from '@/utils/request'
const baseUrl = '/goodSpu'

export function genGoodSpuCode(params) {
  return axios.request({
    url: `${baseUrl}/getSpuNo`,
    method: 'post',
    data: params
  })
}

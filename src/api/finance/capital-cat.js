/*
 * @Descripttion: 财务往来账
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 15:34:14
 */
import axios from '@/utils/request'
const baseUrl = '/fin/capitalCat'

export function saveOrUpdateData(params) {
  return axios.request({
    url: `${baseUrl}/saveData`,
    method: 'post',
    data: params
  })
}

export function delData(params) {
  return axios.request({
    url: `${baseUrl}/delData`,
    method: 'post',
    data: params
  })
}

export function getListData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function getPayOptions(params) {
  return axios.request({
    url: `${baseUrl}/listPayOptions`,
    method: 'post',
    data: params
  })
}

export function getReceOptions(params) {
  return axios.request({
    url: `${baseUrl}/listReceOptions`,
    method: 'post',
    data: params
  })
}

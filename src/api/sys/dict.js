/*
 * @Descripttion: 角色接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-04 08:55:48
 */
import axios from '@/utils/request'
const baseUrl = '/dict'

function format2optionsData(data) {
  return data.map(item => {
    return {
      value: item.code,
      label: item.name
    }
  })
}

export function getDataByParent(params) {
  return axios.request({
    url: `${baseUrl}/getDataByParent`,
    method: 'post',
    data: params
  })
}

/**
 * 销售类型
 */
export function getSaleType() {
  return new Promise((resovle, reject) => {
    getDataByParent('saleType').then(res => {
      if (res.data.flag) {
        resovle(format2optionsData(res.data.data))
      }
    })
  })
}

export function getSaleOrderStatus() {
  return new Promise((resovle, reject) => {
    getDataByParent('saleOrderStatus').then(res => {
      if (res.data.flag) {
        resovle(format2optionsData(res.data.data))
      }
    })
  })
}

/**
 * 审核状态
 */
export function getAuditStatus() {
  return new Promise((resovle, reject) => {
    getDataByParent('auditStatus').then(res => {
      if (res.data.flag) {
        resovle(format2optionsData(res.data.data))
      }
    })
  })
}

/**
 * 关闭状态
 */
export function getCloseStatus() {
  return new Promise((resovle, reject) => {
    getDataByParent('closeStatus').then(res => {
      if (res.data.flag) {
        resovle(format2optionsData(res.data.data))
      }
    })
  })
}

/**
 * 出库状态
 */
export function getStockOutStatus() {
  return new Promise((resovle, reject) => {
    getDataByParent('stockOutStatus').then(res => {
      if (res.data.flag) {
        resovle(format2optionsData(res.data.data))
      }
    })
  })
}

export function getStockInStatus() {
  return new Promise((resovle, reject) => {
    getDataByParent('stockInStatus').then(res => {
      if (res.data.flag) {
        resovle(format2optionsData(res.data.data))
      }
    })
  })
}

export function getInventoryStatus() {
  return new Promise((resovle, reject) => {
    getDataByParent('inventoryStatus').then(res => {
      if (res.data.flag) {
        resovle(format2optionsData(res.data.data))
      }
    })
  })
}

export function getReturnStatus() {
  return new Promise((resovle, reject) => {
    getDataByParent('returnStatus').then(res => {
      if (res.data.flag) {
        resovle(format2optionsData(res.data.data))
      }
    })
  })
}

/**
 * 给数组添加，不限属性
 */
export function unshiftLimit(data) {
  data.unshift({ value: '-1', label: '不限' })
}


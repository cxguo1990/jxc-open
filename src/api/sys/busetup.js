/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-09-15 10:30:58
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 16:18:20
 */
import axios from '@/utils/request'
const baseUrl = '/busetup'

export function saveBusinessSetup(params) {
  return axios.request({
    url: `${baseUrl}/save_business_setup`,
    method: 'post',
    data: params
  })
}

export function getBusinessSetup(params) {
  return axios.request({
    url: `${baseUrl}/get_business_setup`,
    method: 'post',
    data: params
  })
}


/*
 * @Descripttion: 权限接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-07 06:09:07
 */
import axios from '@/utils/request'
const baseUrl = '/permission'

export function listAllData(params) {
  return axios.request({
    url: `${baseUrl}/listAllData`,
    method: 'post',
    data: params
  })
}

export function listDataByRoleId(roleId) {
  return axios.request({
    url: `${baseUrl}/listData/${roleId}`,
    method: 'post',
    data: roleId
  })
}

export function listPricePermissions(params) {
  return axios.request({
    url: `${baseUrl}/listPriceTypeOptions`,
    method: 'post',
    data: params
  })
}


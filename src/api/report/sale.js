/*
 * @Descripttion: 客户接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 09:54:57
 */
import axios from '@/utils/request'
const baseUrl = '/report/sale'

export function getSaleInfo(params) {
  return axios.request({
    url: `${baseUrl}/getSaleInfo`,
    method: 'post',
    data: params
  })
}

export function listSaleGroupByBill(params) {
  return axios.request({
    url: `${baseUrl}/listDataGroupByBillPage`,
    method: 'post',
    data: params
  })
}

export function listSaleGroupByComego(params) {
  return axios.request({
    url: `${baseUrl}/listDataGroupByComegoPage`,
    method: 'post',
    data: params
  })
}

export function listSaleGroupByGoods(params) {
  return axios.request({
    url: `${baseUrl}/listDataGroupByGoodPage`,
    method: 'post',
    data: params
  })
}

export function detailGoodsGroupByBill(params) {
  const skuId = params.data.skuId
  return axios.request({
    url: `${baseUrl}/listGoodDetailGroupByBillPage/${skuId}`,
    method: 'post',
    data: params
  })
}

export function detailGoodsGroupByComego(params) {
  const skuId = params.data.skuId
  return axios.request({
    url: `${baseUrl}/listGoodDetailGroupByComegoPage/${skuId}`,
    method: 'post',
    data: params
  })
}

export function detailComegoGroupByBill(params) {
  const comegoId = params.data.comegoId
  return axios.request({
    url: `${baseUrl}/listComegoDetailGroupByBillPage/${comegoId}`,
    method: 'post',
    data: params
  })
}

export function detailComegoGroupByGoods(params) {
  const comegoId = params.data.comegoId
  return axios.request({
    url: `${baseUrl}/listComegoDetailGroupByGoodPage/${comegoId}`,
    method: 'post',
    data: params
  })
}

export function listBillDetailByDate(params) {
  const date = params.data.date
  return axios.request({
    url: `${baseUrl}/listBillDetail/${date}`,
    method: 'post',
    data: params
  })
}


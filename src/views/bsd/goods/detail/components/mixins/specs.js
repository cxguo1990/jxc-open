/*
 * @Descripttion: 和多规格相关
 * @version:
 * @Author: cxguo
 * @Date: 2019-11-07 15:17:45
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-06 16:03:03
 */
import clonedeep from 'clonedeep'
import { listData as getData } from '@/api/bsd/specs.js'
import XEUtils from 'xe-utils'

export default {
  data() {
    return {}
  },
  props: {
  },
  computed: {
    /**
     * 只有规格的列数据
     */
    specsColumns: function() {
      const isEnableSpecs = this.dataObj.isEnableSpecs
      if (isEnableSpecs === '0') return []
      const specsData = this.specsData
      const hasValueSpecs = specsData.filter(item => { return item.valueData && item.valueData.length > 0 })
      const result = []
      hasValueSpecs.forEach(item => {
        const { id, name } = item
        const colObj = { field: id, title: name, width: 80, align: 'center' }
        result.push(colObj)
      })
      return result
    },
    /**
     * 只有规格数据的表格数据
     */
    spTableData: function() {
      // 1 获取有value值的规格
      const selectSpecsData = this.selectSpecsData
      // 2 遍历有规格的值
      let result = []
      selectSpecsData.forEach((item, index) => {
        result = this.getSpecsTableDataByspecs(result, item)
      })
      return result
    }
  },
  methods: {
    getPriceType() {
      const { configs } = this.appData
      const priceTypes = configs.priceTypes || []
      return priceTypes
    },
    /**
     * 获取规格待选数据
     */
    getSpecsSelectData(isValueArr = true) {
      getData().then(res => {
        if (res.data.flag) {
          const data = res.data.data
          const treeData = XEUtils.toArrayTree(data, {
            key: 'id',
            parentKey: 'pid'
          })
          treeData.forEach(item => {
            if (isValueArr) item.valueData = []
            else item.valueData = null
          })
          this.specsData = treeData
        }
      })
    },
    getPriceBlankData() {
      const { configs } = this.appData
      const priceTypes = configs.priceTypes || []
      const result = {}
      priceTypes.forEach(item => {
        result[item.value] = []
      })
      return result
    },
    /**
     * 根据选择的规格生成对应的数据
     */
    getSpecsTableDataByspecs(initData = [], selectSpecsObj) {
      const _initData = clonedeep(initData)
      const key = Object.keys(selectSpecsObj)[0]
      const value = selectSpecsObj[key]
      const spValueData = value // 选中值
      const spValue2ColData = []
      spValueData.forEach(item => {
        const obj = {}
        obj[key] = item
        spValue2ColData.push(obj)
      })
      const biggerData = _initData.length >= spValue2ColData.length ? _initData : spValue2ColData
      const littleData = _initData.length < spValue2ColData.length ? _initData : spValue2ColData
      const result = []
      biggerData.forEach((big, i) => {
        if (!littleData.length) return result.push(big)
        littleData.forEach((lit, j) => {
          const obj = Object.assign(clonedeep(big), clonedeep(lit))
          result.push(obj)
        })
      })
      return result
    }
  }
}

/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-04 12:37:50
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 15:08:49
 */
import { amount } from '@/libs/tools.js'

export const initDefaultStockData = function(storeData) {
  return initStockData(storeData)
}

export const initUpdateStockData = function(goodsStockList) {
  return initStockData(null, goodsStockList)
}

/**
 * 初始化stock数据
 */
function initStockData(storeData, goodsStockList = []) {
  if (goodsStockList.length > 0) {
    return goodsStockList
  }
  const zero = amount(0).format()
  const result = storeData.map(item => {
    return {
      originQuantity: zero,
      costPrice: zero,
      costTotalPrice: zero
    }
  })
  return result
}

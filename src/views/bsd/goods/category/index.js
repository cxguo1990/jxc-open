/*
 * @Descripttion: 商品树形分类
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-15 20:33:33
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-12 18:08:29
 */
import Main from './main'
import CateTree from './cate-tree'
export default Main
export { CateTree, Main }


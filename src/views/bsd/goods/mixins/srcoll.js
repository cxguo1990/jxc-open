/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2019-08-20 17:29:54
 */
export default {
  data() {
    return {
      offsetTop: 0
    }
  },
  methods: {
    handleScroll() {
      this.offsetTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
    }
  },
  mounted() {
    window.addEventListener('scroll', this.handleScroll)
  },
  beforeDestroy() {
    window.removeEventListener('scroll', this.handleScroll)
  },
  computed: {
    'aside': function() {
      if (this.offsetTop < 50) return 'aside'
      else return 'aside fixed'
    },
    'mainLeft': function() {
      if (this.offsetTop < 50) return '0'
      else return '150px'
    }
  }
}

/*
 * @Descripttion: table列
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-05-30 21:51:07
 */
export default {
  data() {
    return {
      columnsByBill: [
        { title: '序号', width: 60, fixed: 'left', align: 'center',
          slots: {
            default: (params) => {
              const { seq } = params
              const { direction } = params.row
              if (direction === '1') return [<el-tag type='danger'>退货</el-tag>]
              const value = seq
              return [<span>{value}</span>]
            }
          }
        },
        { field: 'businessTime', title: '业务日期', width: 110, showOverflow: true },
        { field: 'billNo', title: '单据编号', width: 150 },
        { field: 'name', title: '商品', width: 200, showOverflow: true },
        { field: 'quantity', title: '销售量', align: 'right', width: 120 },
        { field: 'amountPayable', title: '销售额', align: 'right', width: 100 },
        { field: 'amountDiscount', title: '折扣额', align: 'right', width: 120 },
        { field: 'grossProfit', title: '销售毛利', align: 'right', width: 100,
          slots: {
            default: ({ row }) => {
              const { amountPayable, priceCostTotal } = row
              const value = this.$amount(amountPayable)
                .subtract(priceCostTotal).format()
              return [<span>{value}</span>]
            }
          }
        },
        { field: 'grossProfit', title: '毛利率', align: 'right', width: 100,
          slots: {
            default: ({ row }) => {
              const { amountPayable, priceCostTotal } = row
              const grossProfit = this.$amount(amountPayable)
                .subtract(priceCostTotal).format()
              const value = (Math.round(grossProfit * 10000 / amountPayable) / 100) + '%'
              return [<span>{value}</span>]
            }
          }
        }
      ],
      columnsByGood: [
        { title: '序号', width: 60, fixed: 'left', align: 'center',
          slots: {
            default: (params) => {
              const { seq } = params
              const value = seq
              return [<span>{value}</span>]
            }
          }
        },
        { field: 'code', title: '商品编号', width: 200, showOverflow: true },
        { field: 'name', title: '商品名称', width: 200, showOverflow: true },
        { field: 'unitName', title: '单位', width: 120 },
        { field: 'quantity', title: '销售量', width: 120 },
        { field: 'amountPayable', title: '销售额', align: 'right', width: 120 },
        { field: 'amountDiscount', title: '折扣额', align: 'right', width: 120 },
        { field: 'amountSaleAvg', title: '销售均价', align: 'right', width: 120,
          slots: {
            default: ({ row }) => {
              const { amountPayable, quantity } = row
              const value = this.$amount(amountPayable)
                .divide(quantity).format()
              return [<span>{value}</span>]
            }
          }
        },
        { field: 'grossProfit', title: '销售毛利', align: 'right', width: 100,
          slots: {
            default: ({ row }) => {
              const { amountPayable, priceCostTotal } = row
              const value = this.$amount(amountPayable)
                .subtract(priceCostTotal).format()
              return [<span>{value}</span>]
            }
          }
        }
      ]
    }
  },
  methods: {
  }
}


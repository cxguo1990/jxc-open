
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-11 20:20:02
 */
import { getBillAndDetail as getBillContainGoodsData } from '@/api/bill/bill-sale-return.js'
import { getBillcode } from '@/api/bill/bill-sale-return.js'
import moment from 'moment'

export default {
  data() {
    return {}
  },
  methods: {
    initUpdateData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billDetailList } = data
        this.dataObj = bill
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    initViewData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billDetailList } = data
        this.dataObj = bill
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    initTransData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billDetailList } = data
        bill.purchasebillNo = bill.billNo
        bill.billRelationId = bill.id
        bill.billRelationNo = bill.billNo
        delete bill.billNo
        delete bill.id
        billDetailList.forEach(item => {
          const { changeQuantity, returnQuantity } = item
          const canReturnQty = this.$amount(changeQuantity).subtract(returnQuantity).format()
          this.$set(item, 'quantity', canReturnQty)
          this.$set(item, 'maxq', canReturnQty)
        })
        const date = moment().format('YYYY-MM-DD HH:mm:ss')
        bill.businessTime = date
        this.setDataObj(bill)
        this.$refs.GoodsSelect.setTableData(billDetailList)
        this.getBillCode()
      })
    },
    setDataObj(dataObj) {
      Object.keys(dataObj).forEach(key => {
        this.$set(this.dataObj, key, dataObj[key])
      })
    },
    getBillCode() {
      getBillcode().then(res => {
        if (!res.data.flag) this.$message.error('获取票据编号失败！')
        const code = res.data.data
        this.$set(this.dataObj, 'billNo', code)
      })
    },
    getBillContainGoodsData(billId) {
      return new Promise((resolve, reject) => {
        getBillContainGoodsData(billId).then(res => {
          if (!res.data.flag) {
            return reject(new Error('获取采购单商品失败！'))
          }
          const data = res.data.data
          resolve(data)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
}



/*
 * @Descripttion: 价格的变化
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-24 11:04:39
 */
export default {
  computed: {
    'priceCats': function() {
      return ['1_0', '1_1']
    },
    'defaultPriceCat': function() {
      const saleType = this.billExtSale.type
      if (saleType === '0') return '1_1'
      return '1_0'
    },
    'defaultPriceCatName': function() {
      const saleType = this.billExtSale.type
      if (saleType === '0') return '零售价'
      return '批发价'
    }
  }
}


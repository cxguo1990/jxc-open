
/*
 * @Descripttion: 对外开放的api
 * @version:
 * @Author: cxguo
 * @Date: 2019-10-18 15:37:24
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-09 09:47:27
 */

export default {
  methods: {
    /**
     * 设置表格数据
     * @param {*} dataArr
     */
    setTableData(dataArr) {
      this.tableData = dataArr
    },
    /**
     * 在现有的数据上新增
     */
    pushTableData(data) {
      const tableData = this.tableData
      const blankData = this.tableData.filter(item => {
        return !item.skuId
      })
      const blankLen = blankData.length
      const hasLen = tableData.length - blankLen
      const needLen = data.length
      // 补空位
      if (blankLen < needLen) {
        const disLen = needLen - blankLen
        const blankDataList = this.getBlankDataList(disLen)
        this.tableData.push(...blankDataList)
      }
      // 赋值
      for (let i = hasLen; i < needLen + hasLen; i++) {
        this.updateTableData(i, data[i - hasLen])
      }
    },
    /**
     * 获取要保存的数据
     */
    getPostData() {
      const tableData = this.getTableData()
      const postData = tableData.filter(item => { return item.skuId })
      return postData
    },
    async valiData() {
      const postData = this.getPostData()
      const errMap = await this.$refs.table.validate(postData).catch(errMap => errMap)
      return new Promise((resovel, reject) => {
        if (errMap) {
          reject(errMap)
        } else {
          resovel()
        }
      })
    },
    setTableLoading(val) {
      this.loading = val
    }
  }
}


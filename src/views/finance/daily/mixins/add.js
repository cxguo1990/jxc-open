
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-08 14:13:50
 */
import moment from 'moment'
import { getBillCode as getBillIncode } from '@/api/finance/daily-in.js'
import { getBillCode as getBillOutcode } from '@/api/finance/daily-out.js'

export default {
  data() {
    return {}
  },
  methods: {
    initSaveData(direction) {
      this.initOptions()
      this.getBillCode(direction)
      const date = moment().format('YYYY-MM-DD HH:mm:ss')
      this.$set(this.dataObj, 'businessTime', date)
    },
    initOptions() {
      this.getCapitalCat()
    },
    getBillCode(direction) {
      let method = null
      if (direction === '0') method = getBillOutcode
      if (direction === '1') method = getBillIncode
      method().then(res => {
        if (!res.data.flag) this.$message.error('获取票据编号失败！')
        const code = res.data.data
        this.$set(this.dataObj, 'billNo', code)
      })
    }
  }
}


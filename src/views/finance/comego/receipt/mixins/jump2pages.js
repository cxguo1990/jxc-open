/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2019-10-09 14:09:04
 */
export default {
  data() {
    return {}
  },
  methods: {
    jump2purchaseDetail({ purchaseId, operation }) {
      if (purchaseId) {
        this.$router.push({
          name: 'PurchaseDetailIn',
          query: { purchaseId, operation }
        })
      } else {
        this.$router.push({
          name: 'PurchaseDetailIn',
          query: { operation }
        })
      }
    },
    /**
     * 跳转到采购退货单明细
     * @param {*} param0
     */
    jump2purchaseOutDetail({ purchaseId, operation }) {
      if (purchaseId) {
        this.$router.push({
          name: 'PurchaseDetailOut',
          query: { purchaseId, operation }
        })
      } else {
        this.$router.push({
          name: 'PurchaseDetailOut',
          query: { operation }
        })
      }
    }
  }
}


/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:24:45
 * @LastEditors: cxguo
 * @LastEditTime: 2019-12-18 15:31:02
 */
import jump2pages from './jump2pages'
import add from './add'
import update from './update'
import columns from './columns'
export { add, update, columns, jump2pages }

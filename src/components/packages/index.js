/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-05-27 13:28:09
 * @LastEditors: cxguo
 * @LastEditTime: 2020-03-04 20:58:51
 */
import Iframe from './iframe'

class Layer {
  constructor(optionis) {
    this.iframe = new Iframe(optionis)
  }
}
const layer = (optionis) => {
  return new Layer(optionis)
}
layer.install = (Vue, optionis) => {
  Vue.prototype.$layer = layer(optionis)
}
export default layer

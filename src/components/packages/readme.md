#1. 快速开始
在程序入口文件添加:
import Vue from 'vue';
import layer from '_c/layer'
Vue.prototype.$layer = layer(Vue);

#2. 全局配置信息
Vue.prototype.$layer = layer(Vue,{
  iframeTitleHeight:30 //iframe标题的高度
});

#3. 方法: add添加(返回iframe对象), removeAll删除所有的iframe
layer.iframe.add({
    content: {
      content: component, //传递的组件对象
      parent: this, //当前的vue对象
      props:{} //组件的props
    },
    area:['800px','600px'], //大小
    position:['10px','10px'], //位置
    title: 'title' //标题名字
});
layer.iframe.removeAll();

#4. iframe对象
[方法]
close: 关闭iframe
min: 缩小iframe
max: 放大iframe
[事件]
on-click-title: 点击标题: 返回iframe
on-close: 关闭
on-min: 缩小
on-max: 放大
on-iframecontent-nexttick: 传入的vue组件渲染完成, 返回传入的组件对象
一般用到 on-close, on-iframecontent-nexttick(方面操作vue组件)

#5. 注意事项
因为layer-iframe-container是在body而不是#app下面
所以: 在引入的vue中如果需要使用全局的对象 (比如Vuex):
只能通过: Vue.prototype.$objName = objName 的方式使用
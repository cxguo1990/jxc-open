/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-05-27 13:28:09
 * @LastEditors: cxguo
 * @LastEditTime: 2020-03-05 16:07:25
 */
import IframeVmObj from './iframe'
import { parseDom, getIframeName } from '../utils/tool'
import { CONTAINER_CLASS_NAME, GLOBAL_DEFAULT_PROPERTIES } from './config'
import Vue from 'vue'

class Iframe {
  constructor(globalOptions) {
    // [初始化]
    // 所有iframe的集合, 用obj方便直接根据名字查找iframe
    // this.iframes = {}
    // iframe在最小状态的位置(设计思路 -> 添加座位.落座)
    this.minPositions = {}
    // 配置iframe的全局参数 (Vue, iframe_title_height)
    this.globalOptions = globalOptions
  }
  getDefaultContainer() {
    const layerIframe = parseDom('<div id=' + CONTAINER_CLASS_NAME + '></div>')[0]
    document.body.appendChild(layerIframe)
    const container = document.getElementById(CONTAINER_CLASS_NAME)
    this.container = container
    return container
  }
  /**
   * 添加iframe
   */
  add(properties) {
    const iframeName = properties.name ? properties.name : getIframeName()
    const propsComponents = properties
    const globalOptions = Object.assign(GLOBAL_DEFAULT_PROPERTIES, this.globalOptions)
    const propsTotal = Object.assign(
      globalOptions,
      propsComponents,
      {
        iframes: this.iframes,
        minPositions: this.minPositions,
        name: iframeName
      }
    )
    return this.amountIframe(propsTotal)
  }
  amountIframe(propsTotal) {
    const Profile = Vue.extend(IframeVmObj)
    const Instance = Object.assign(new Profile(), propsTotal)
    const iframe = Instance.$mount()
    // 添加到容器
    const propContainer = propsTotal.container
    const container = this.container || propContainer || this.getDefaultContainer()
    container.appendChild(iframe.$el)
    // 加入iframe集合
    // this.iframes[iframeName] = iframe
    // 组件销毁后执行操作
    iframe.$on('hook:beforeDestroy', () => {
      // this.iframes[iframe.name] = null
      // delete this.iframes[iframe.name] // 清除iframes集合中对象
    })
    return iframe
  }
  addSingle(properties) {
    if (!properties.name) throw new Error('addSingle方法必须有name')
    const name = properties.name
    if (this.iframes[name]) return this.iframes[name]
    else return this.add(properties)
  }
  create(properties) {
    this.add(properties)
  }
  removeAll() {
    const iframeObjs = this.iframes
    Object.keys(iframeObjs).forEach(key => {
      const iframe = iframeObjs[key]
      iframe.close()
    })
    this.iframes = {}
    this.minPositions = {}
    this.container.innerHTML = ''
  }
}
export default Iframe

/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-10-19 14:44:41
 * @LastEditors: cxguo
 * @LastEditTime: 2019-10-30 13:50:50
 */

import installGoodsSelectRender from './goods-select-render'

/**
 * 安装Vxe的render渲染器
 * @param {*} VXETable
 */
export const installVxeRenders = function(VXETable) {
  installGoodsSelectRender(VXETable)
}

/**
 * 获取表格Index索引 - Header部分的图标VNode
 * @param {*} column
 */
export const getTableIndexColHeader = function({ column, events }) {
  const data = {
    on: {
      'click': events.click
    }
  }
  // eslint-disable-next-line no-unused-vars
  const h = this.$createElement
  return [
    <a class='el-icon-s-operation' {...data}/>
  ]
}

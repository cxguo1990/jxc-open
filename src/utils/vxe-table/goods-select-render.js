/*
Descripttion:

* @version:
 * @Author: cxguo
 * @Date: 2019-10-19 18:03:29
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-27 14:59:29
 */
import XEUtils from 'xe-utils'

function getElInput(h, cellRender, $table) {
  const { row, column } = $table
  const { events, props } = cellRender
  const inputData = {
    props: props,
    model: {
      value: XEUtils.get(row, column.property),
      callback(value) {
        XEUtils.set(row, column.property, value)
      }
    },
    on: {
      'change': (currentValue, oldValue) => {
        events && events.change && events.change(row, { currentValue, oldValue })
      }
    },
    nativeOn: {
      'input': (e) => {
        events && events.input && events.input(e, $table)
      },
      'blur': (e) => {
        console.log('失焦：', e)
        // events && events.input && events.blur(e)
      },
      'keydown': (e) => {
        events && events.keydown && events.keydown(e)
      }
    }
  }
  return inputData
}

export default (VXETable) => {
  VXETable.renderer.add('ElInputShowBySure', {
    autofocus: 'input.el-input__inner',
    renderCell(h, cellRender, $table) {
      const { row, column } = $table
      const value = row[column.property]
      if (!row.skuId) return <span>{value}</span>
      return [
        <el-input {...getElInput(h, cellRender, $table)}></el-input>
      ]
    }
  })

  VXETable.renderer.add('CxInputNumber', {
    autofocus: 'input.el-input__inner',
    renderCell(h, cellRender, params) {
      const { row, column } = params
      const value = row[column.property]
      return [value]
    },
    renderEdit(h, editRender, params) {
      const { row, column, $table } = params
      const { events, props } = editRender
      const inputData = {
        props: {
          value: row[column.property]
        },
        on: {
          input(value) {
            const result = value.replace(/[^\d.]/g, '') || 0
            row[column.property] = result
            $table.updateStatus(params)
          },
          blur: (e) => {
            events && events.onBlur && events.onBlur(e, $table)
          }
        }
      }
      if (props) {
        if (props.showCanUseQty) {
          const content = `可用库存${row.canUseQty}`
          const tooltipData = {
            props: { content, placement: 'top' }
          }
          return [
            <el-tooltip {...tooltipData}>
              <el-input {...inputData}></el-input>
            </el-tooltip>
          ]
        }
      }
      return [
        <el-input {...inputData}></el-input>
      ]
    }
  })
}

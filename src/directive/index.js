/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-19 09:17:15
 * @LastEditors: cxguo
 * @LastEditTime: 2019-08-19 09:18:05
 */
import directive from './directives'

const importDirective = Vue => {
  /**
   * 拖拽指令 v-draggable="options"
   * options = {
   *  trigger: /这里传入作为拖拽触发器的CSS选择器/,
   *  body:    /这里传入需要移动容器的CSS选择器/,
   *  recover: /拖动结束之后是否恢复到原来的位置/
   * }
   */
  Vue.directive('draggable', directive.draggable)

  /**
   * clipboard指令 v-draggable="options"
   * options = {
   *  value:    /在输入框中使用v-model绑定的值/,
   *  success:  /复制成功后的回调/,
   *  error:    /复制失败后的回调/
   * }
   */
  Vue.directive('clipboard', directive.clipboard)

  /**
   * 权限判断
   * 指令 v-perm="plan:scatt:query"
   */
  Vue.directive('perm', directive.perm)

  /**
   * 是否有角色权限
   */
  Vue.directive('role', directive.role)

  /**
   * form界面输入框，在点enter时候切换到下一个获取焦点
   * 指令 v-enterfocus
   */
  Vue.directive('enterfocus', directive.enterfocus)
}

export default importDirective

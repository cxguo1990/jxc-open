/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-12-13 17:04:37
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 15:52:57
 */
import Layout from '@/layout'

export default {
  path: '/basedata',
  component: Layout,
  redirect: 'noRedirect',
  meta: { title: '资料', icon: 'ziliao' },
  name: 'BaseData',
  children: [
    {
      path: 'goods_list',
      name: 'GoodsList',
      component: () => import('@/views/bsd/goods'),
      meta: { title: '商品', pid: 'basedata1', add: true, perm: 'good:list' }
    },
    {
      path: 'goods_add',
      name: 'GoodAdd',
      hidden: true,
      component: () => import('@/views/bsd/goods/detail'),
      meta: { title: '新增商品', pid: 'basedata1', perm: 'good:adedit' }
    },
    {
      path: 'goods_copyadd',
      name: 'GoodCopyAdd',
      hidden: true,
      props: (route) => ({ oper: 'copyadd', skuid: route.query.skuid }),
      component: () => import('@/views/bsd/goods/detail/sku-edit.vue'),
      meta: { title: '复制新增商品', pid: 'basedata1', perm: 'good:adedit' }
    },
    {
      path: 'goods_import',
      name: 'GoodImport',
      hidden: true,
      component: () => import('@/views/bsd/goods/import'),
      meta: { title: '导入商品数据', pid: 'basedata1', perm: 'good:import' }
    },
    {
      path: 'goods_sku_view',
      name: 'GoodsSkuView',
      hidden: true,
      props: (route) => ({ skuid: route.query.skuid }),
      component: () => import('@/views/bsd/goods/detail/sku-view.vue'),
      meta: { title: '商品SKU详情', pid: 'basedata1', perm: 'good:adedit' }
    },
    {
      path: 'goods_sku_edit',
      name: 'GoodsSkuEdit',
      hidden: true,
      props: (route) => ({ oper: 'edit', skuid: route.query.skuid }),
      component: () => import('@/views/bsd/goods/detail/sku-edit.vue'),
      meta: { title: '商品SKU编辑', pid: 'basedata1', perm: 'good:adedit' }
    },
    {
      path: 'goods_update',
      name: 'GoodsUpdate',
      hidden: true,
      component: () => import('@/views/bsd/goods/detail'),
      meta: { title: '编辑商品', pid: 'basedata1', perm: 'good:adedit' }
    },
    {
      path: 'goods_cat',
      name: 'GoodCatList',
      component: () => import('@/views/bsd/goods/category/main.vue'),
      meta: { title: '商品分类', pid: 'basedata1', perm: 'goodcat:list' }
    },
    {
      path: 'specs_list',
      name: 'SpecsList',
      component: () => import('@/views/bsd/specs'),
      meta: { title: '规格配置', pid: 'basedata1', perm: 'specs:list' }
    },
    {
      path: 'unit_list',
      name: 'UnitList',
      component: () => import('@/views/bsd/unit'),
      meta: { title: '单位配置', pid: 'basedata1', perm: 'unit:list' }
    },
    {
      path: 'customer_list',
      name: 'CustomerList',
      component: () => import('@/views/bsd/customer/list'),
      meta: { title: '客户', pid: 'basedata2', add: true, perm: 'customer:list' }
    },
    {
      path: 'customer_add',
      name: 'CustomerAdd',
      hidden: true,
      component: () => import('@/views/bsd/customer/detail'),
      meta: { title: '新增客户', pid: 'basedata2', perm: 'customer:adedit' }
    },
    {
      path: 'customer_edit',
      name: 'CustomerEdit',
      hidden: true,
      props: (route) => ({ id: route.query.id }),
      component: () => import('@/views/bsd/customer/detail'),
      meta: { title: '编辑客户', pid: 'basedata2', perm: 'customer:adedit' }
    },
    {
      path: 'customer_import',
      name: 'CustomerImport',
      hidden: true,
      component: () => import('@/views/bsd/customer/import'),
      meta: { title: '导入客户资料', pid: 'basedata2', perm: 'customer:import' }
    },
    {
      path: 'supplier_list',
      name: 'SupplierList',
      component: () => import('@/views/bsd/supplier'),
      meta: { title: '供应商', pid: 'basedata2', add: true, perm: 'supplier:list' }
    },
    {
      path: 'supplier_import',
      name: 'SupplierImport',
      hidden: true,
      component: () => import('@/views/bsd/supplier/import'),
      meta: { title: '导入供应商资料', pid: 'basedata2', add: true, perm: 'supplier:import' }
    },
    {
      path: 'supplier_edit',
      name: 'SupplierEdit',
      hidden: true,
      props: (route) => ({ id: route.query.id }),
      component: () => import('@/views/bsd/supplier/detail'),
      meta: { title: '编辑供应商', pid: 'basedata2', perm: 'supplier:adedit' }
    },
    {
      path: 'supplier_add',
      name: 'SupplierAdd',
      hidden: true,
      component: () => import('@/views/bsd/supplier/detail'),
      meta: { title: '新增供应商', pid: 'basedata2', perm: 'supplier:adedit' }
    }
  ]
}
